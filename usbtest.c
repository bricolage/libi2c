#include <stdio.h>
#include <string.h>
#include "libi2c.h"

int 
main()
{
	struct i2cdev *i = i2c_open("19640001", 0x08);
	unsigned char *data = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789\0";
	
	unsigned char buf[512];
	int len = strlen(data)+1;
	if (i > 0)
	{
		int ret = i2c_recv(i, &buf[0], len);
		printf("recv ret: %i,%s\n", ret, &buf[0]);
		
		ret = i2c_send(i, data, len);
		printf("send ret: %i\n", ret);
		
		ret = i2c_recv(i, &buf[0], len);
		printf("recv ret: %i,%s\n", ret, &buf[0]);

		ret = i2c_send(i, data, len);
		printf("send ret: %i\n", ret);
	
	}
	int ic = i2c_close(i);
	printf("close: %i\n", ic);
	return ic;
}

