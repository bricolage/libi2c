/* Copyright(C) 2006,2007,2008 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <usb.h>
#include "libi2c.h"
#define LCKFILE "/dev/shm/usb2i2c.lck"

/* open /dev/i2c device and set things up to be ready for smbus bus 
 * communication.
 * returns i2cdev handle pointer of i2c device or 0 on error
 */
struct i2cdev*
smbus_open(const char *device, int addr)
{
	return i2c_open(device, addr);
}

/*close the device*/
int
smbus_close(struct i2cdev *handle)
{
	return i2c_close(handle);
}


/* send txdata to the usb2i2c bus via smbus protocol 
 * return len on success, -1 on failure.
 */
int
smbus_send(struct i2cdev *handle, int command, const unsigned char *txdata, unsigned int datalen)
{ 
	unsigned char buf[35];
	if(datalen < 36){
		buf[0] = command;
		buf[1] = datalen;
		memcpy(&buf[2], txdata, datalen);
		if(i2c_send(handle, &buf[0], datalen+2) != (datalen+2)){
			return -1; /* ged nid */
		}
			return datalen;
	}
	return -1; /* umpf */
}

/* read data from the usb2i2c bus via smbus protocol 
 * return len on success, -1 on failure
 */
int
smbus_recv(struct i2cdev *handle, int command, unsigned char *rxdata, unsigned int *recvlen)
{
	unsigned char wbuf[1];
	unsigned char rbuf[35];
	wbuf[0] = command;
	if(i2c_send(handle, &wbuf[0], 1) != 1){
		return -1; /* ged nid */
	}
	if(i2c_recv(handle, &rbuf[0], 33) != 33)
		return -1; /* stop. */
	*recvlen = (int) rbuf[0];
	memcpy(rxdata, &rbuf[1], 32);
	return *recvlen; /* jeepy */
}



