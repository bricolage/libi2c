/* Copyright(C) 2006,2007,2008 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/


#ifndef _LIBSMBUS
#define _LIBSMBUS

struct i2cdev; /* device handle zeiger */

/* open /dev/i2c device and set things up to be ready for smbus bus 
 * communication.
 * returns unix file descriptor of i2c device or 0 on error
 */
struct i2cdev*
smbus_open(char *device, int addr);

/*close the device*/
int
smbus_close(struct i2cdev *handle);


/* send txdata to the smbus
 * return datalen on success, <= 0 on failure.
 */
int 
smbus_send(struct i2cdev *handle, int command, const unsigned char *txdata, unsigned int datalen);

/* read data from smbus 
 * return > 0 on success, <= 0 on failure
 */
int
smbus_recv(struct i2cdev *handle, int command, unsigned char *rxdata, unsigned int *recvlen);

#endif /* ... _SMBUS_KERNEL */
