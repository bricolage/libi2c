/* Copyright(C) 2007,2008 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <regex.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>

#define MAXTRY 5
#define MAXTRYOPENCLOSE 8
#define TIMEWAIT 3
#define I2C_DATAOFFSET 4
//#define DEBUG
//#define OPENDEBUG


enum datalen {
  GETMAXDATA = 1,
  READFROMI2C
};

struct i2cdev {
	int sock;
	int addr;
	unsigned char seqnum;
	unsigned int maxdatapaketlen;
};

/*
 * i2c packet format
 */
struct i2c_packet_maxdatalen_tx {
	unsigned char seqnum;
};

struct i2c_packet_write_tx {
	unsigned char seqnum;
	unsigned char addrrw; // i2c addr and rw flag , packet tx
	unsigned char writedata[0];
};

struct i2c_packet_write_rx {
	unsigned char seqnum;
	union{
		unsigned char maxdatapaketlen;
		unsigned char writelen;
	};
};

struct i2c_packet_read_tx {
	unsigned char seqnum;
	unsigned char addrrw; // i2c addr and rw flag , packet tx
	unsigned char readdatalen;
};

struct i2c_packet_read_rx {
	unsigned char seqnum;
	unsigned char readdata[0];
};



/* socket oeffnen und mit ethersex verbinden */
struct i2cdev*
i2c_open(const char *device, int addr)
{
	struct i2cdev *handle;
	struct addrinfo hints, *res, *res0;
	int error;
	const char *cause = NULL;

	if((handle = malloc(sizeof(*handle))) == NULL){
		perror("malloc");
		return 0;
	}
	unsigned int port;
	char *hoststr = NULL;
	if ((hoststr = strdup(device)) == NULL){
		perror("malloc");
		return 0;
	}
	regex_t regex;
	regmatch_t matches[1];
	const char *regexstring = ":";
	regcomp(&regex, regexstring, 0);
	if (0 != regexec(&regex, hoststr, 1, matches, 0)){
		fprintf(stderr, "keinen host/port gefunden\n");
		return 0;
	}
	hoststr[matches[0].rm_so] = 0;
	sscanf(&hoststr[matches[0].rm_eo], "%i", &port);
	if (port < 1 || port > 0xFFFF){
	fprintf(stderr, "kein port gefunden: %i\n", port);
		return 0;
	}
#if defined(DEBUG) || defined(OPENDEBUG)
	fprintf(stderr, "port %s, host %s\n",&hoststr[matches[0].rm_eo] , hoststr);
#endif
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	error = getaddrinfo(hoststr, &hoststr[matches[0].rm_eo], &hints, &res0);
	if (error) {
		errx(1, "%s", gai_strerror(error));
		/*gibts nid*/
	}
	handle->sock = -1;
	for (res = res0; res; res = res->ai_next) {
		handle->sock = socket(res->ai_family, res->ai_socktype,
		res->ai_protocol);
		if (handle->sock < 0) {
			cause = "socket";
			printf("hab kein socket\n");
			continue;
		}
	
		if (connect(handle->sock, res->ai_addr, res->ai_addrlen) < 0) {
			cause = "connect";
			printf("hab kein connect\n");
			close(handle->sock);
			handle->sock = -1;
			continue;
		}
#if defined(DEBUG) || defined(OPENDEBUG)
		fprintf(stderr, "hab verbindung\n");
#endif
		break;  /* juhuu hab ne verbindung, ran an die arbeit */
	}
	if (handle->sock < 0) {
		printf("Socket error: %s", cause);
		return 0;
		/*keine verbindung*/
	}
	handle->addr = addr;
	handle->seqnum = 0;
	handle->maxdatapaketlen = 0;

	fd_set fds;
	struct timeval timeout;
	int readlen = 0;
	int maxtry = 0;

	struct i2c_packet_maxdatalen_tx i2cpacket_tx;
	struct i2c_packet_write_rx i2cpacket_rx;

	do{
		/* hole die maximale paketlaenge */
		i2cpacket_tx.seqnum = 0;
		if(write(handle->sock, &i2cpacket_tx, sizeof(struct i2c_packet_maxdatalen_tx)) != sizeof(struct i2c_packet_maxdatalen_tx)) {
			fprintf(stderr,"i2c open error\n");
			return 0; /* schlug fehl */
		}

		timeout.tv_sec = 5;
		timeout.tv_usec = 0;

		FD_ZERO(&fds);
		FD_SET(handle->sock, &fds);
		select(handle->sock+1, &fds, NULL, NULL, &timeout);
#if defined(DEBUG) || defined(OPENDEBUG)
		fprintf(stderr,"select returned, tv_sec=%u, tv_used=%u\n", timeout.tv_sec, timeout.tv_usec);
#endif
		if(FD_ISSET(handle->sock, &fds) == 1){
			readlen = read(handle->sock, ((unsigned char *) &i2cpacket_rx), sizeof(struct i2c_packet_write_rx));
			if (readlen == sizeof(struct i2c_packet_write_rx) && i2cpacket_rx.seqnum == 0 
				&& i2cpacket_rx.maxdatapaketlen > 0){
					handle->maxdatapaketlen = i2cpacket_rx.maxdatapaketlen;
#if defined(DEBUG) || defined(OPENDEBUG)
					fprintf(stderr,"i2c opened (readlen: %i, maxdatapaketlen: %i)\n", readlen, handle->maxdatapaketlen);
#endif
			}
		}
	
	}while(handle->maxdatapaketlen == 0 && maxtry++ < MAXTRYOPENCLOSE);
	if (maxtry >= MAXTRYOPENCLOSE){
#if defined(DEBUG) || defined(OPENDEBUG)
		fprintf(stderr,"TRYOUT %i\n", maxtry);
#endif
		return 0;
	}
	return handle;
}

/* socket schliessen */
int
i2c_close(struct i2cdev *handle)
{
#if defined(DEBUG) || defined(OPENDEBUG)
	fprintf(stderr,"versuche close\n");
#endif
	if(handle != NULL)
	{
		close(handle->sock);
		free(handle);
		return 1;
	}
	return -1;
}

/* send data to i2c device
 */
int
i2c_send(struct i2cdev *handle, const unsigned char *data, int len)
{
	/* Adresse mit Daten und RW Flag verbinden und senden */
	fd_set fds;
	struct timeval timeout;
	
	struct i2c_packet_write_rx i2cpacket_rx;
	struct i2c_packet_write_tx *i2cpacket_tx;

	if((i2cpacket_tx = malloc(sizeof(struct i2c_packet_write_tx) + handle->maxdatapaketlen)) == NULL){
		perror("malloc");
		return -1;
	}

	int readlen = 0;
	int aktlen = len;
	int sendlen = 0;
	int maxseqnum = 0;
	int aktpos = 0;
	
	if(len > handle->maxdatapaketlen){
		maxseqnum = len / handle->maxdatapaketlen;
		handle->seqnum = maxseqnum; 
	}
	else{
		handle->seqnum = 0;
	}
	
	do{
		int maxtry = 0;
		i2cpacket_tx->seqnum = handle->seqnum;
		i2cpacket_tx->addrrw = (handle->addr << 1) & 0xFE;
#ifdef DEBUG
		fprintf(stderr,"i2c send - addrrw: %02X\n", i2cpacket_tx->addrrw);
#endif

		if(maxseqnum > 0){
			aktpos = (maxseqnum - handle->seqnum) * handle->maxdatapaketlen;
			if(handle->seqnum > 0){
				aktlen = handle->maxdatapaketlen;
			}
			else{
				aktlen = len % handle->maxdatapaketlen;
			}
		}
#ifdef DEBUG
		fprintf(stderr,"i2c send - seqnum: %i, maxseqnum %i, aktpos %i, aktlen: %i, maxdatapaketlen: %i\n", handle->seqnum, maxseqnum, aktpos, aktlen, handle->maxdatapaketlen);
#endif

		memcpy(&i2cpacket_tx->writedata,data+aktpos,aktlen);
		
		do{
		/* sende request und daten */
			if(write(handle->sock, i2cpacket_tx, aktlen + sizeof(struct i2c_packet_write_tx)) != aktlen + sizeof(struct i2c_packet_write_tx))
				return -1; /* schlug fehl */
		/* empfange die antwort */
			timeout.tv_sec = TIMEWAIT;
			timeout.tv_usec = 0;

			FD_ZERO(&fds);
			FD_SET(handle->sock, &fds);
			select(handle->sock+1, &fds, NULL, NULL, &timeout);
			
			if(FD_ISSET(handle->sock, &fds) == 1){
				readlen = read(handle->sock, ((unsigned char *) &i2cpacket_rx), sizeof(struct i2c_packet_write_rx));
				if (readlen == 1) //error und raus
					break;
			}
		}while(readlen != 2 && maxtry++ < MAXTRY);

		sendlen += i2cpacket_rx.writelen;
#ifdef DEBUG
		fprintf(stderr,"i2c sended - readlen: %i, seqnum: %i, readdata %i\n", readlen, i2cpacket_rx.seqnum, i2cpacket_rx.writelen);
#endif
		
		if(readlen == 1){
			free(i2cpacket_tx);
			return -1; /* schreib fehler am i2cbus */
		}
	}while(handle->seqnum-- > 0);

	free(i2cpacket_tx);

	return (int)sendlen; /* laenge zurueckgeben */
}



/* read data from i2c device
 */
int
i2c_recv(struct i2cdev *handle, unsigned char *buf, int len)
{
	fd_set fds;
	struct timeval timeout;

	struct i2c_packet_read_tx i2cpacket_tx;
	struct i2c_packet_read_rx *i2cpacket_rx;
	
	if((i2cpacket_rx = malloc(sizeof(struct i2c_packet_read_rx) + handle->maxdatapaketlen)) == NULL){
		perror("malloc");
		return -1;
	}

	int errorcount = 0;
	int recvlen = 0;

	while(recvlen != len && errorcount < 5){

		int readlen = 0;
		int aktlen = len;
		recvlen = 0;
		int maxseqnum = 0;
		int aktpos = 0;

		if(len > handle->maxdatapaketlen){
			maxseqnum = len / handle->maxdatapaketlen;
			handle->seqnum = maxseqnum; 
		}
		else{
			handle->seqnum = 0;
		}
	
		do{
			i2cpacket_tx.seqnum = handle->seqnum;
			i2cpacket_tx.addrrw = (handle->addr << 1) | 0x01;
#ifdef DEBUG
			fprintf(stderr,"i2c recv - addrrw: %02X\n", i2cpacket_tx.addrrw);
#endif

			if(maxseqnum > 0){
				aktpos = (maxseqnum - handle->seqnum) * handle->maxdatapaketlen;
				if(handle->seqnum > 0){
					aktlen = handle->maxdatapaketlen;
				}
				else{
					aktlen = len % handle->maxdatapaketlen;
				}
			}
			i2cpacket_tx.readdatalen = aktlen;
#ifdef DEBUG
			fprintf(stderr,"i2c recv - seqnum: %i, aktpos %i, aktlen: %i, maxdatapaketlen: %i\n", handle->seqnum, aktpos, aktlen, handle->maxdatapaketlen);
#endif
			/* sende request */
			if(write(handle->sock, &i2cpacket_tx, sizeof(struct i2c_packet_read_tx)) != sizeof(struct i2c_packet_read_tx)){
				free(i2cpacket_rx);
				return -1; /* schlug fehl */
			}
			/* empfange die daten */
			timeout.tv_sec = TIMEWAIT;
			timeout.tv_usec = 0;

			FD_ZERO(&fds);
			FD_SET(handle->sock, &fds);
			select(handle->sock+1, &fds, NULL, NULL, &timeout);
			
			if(FD_ISSET(handle->sock, &fds) == 1){
				readlen = read(handle->sock, ((unsigned char *) i2cpacket_rx), sizeof(struct i2c_packet_read_rx) + aktlen);
			}
#ifdef DEBUG
			fprintf(stderr,"i2c recved - readlen: %i, seqnum: %i, readdata %i\n", readlen, i2cpacket_rx->seqnum, i2cpacket_rx->readdata[0]);
#endif
		
			if(readlen != (sizeof(struct i2c_packet_read_rx) + aktlen)){
				errorcount++;
#ifdef DEBUG
				fprintf(stderr,"i2c recved - errorcount %i\n", errorcount);
#endif
				break;
			}

			memcpy(buf+aktpos, i2cpacket_rx->readdata, aktlen);
			recvlen += readlen - 1;

		}while(handle->seqnum-- > 0);
	}//while


	free(i2cpacket_rx);
	if(errorcount == 5)
		return -1;

	return recvlen;
}
