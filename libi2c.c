/* Copyright(C) 2006,2007,2008 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/
 
/* loader for usb or kernel lib */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>
#include <string.h>
#include <sys/types.h>
#include <regex.h>


struct i2cdev {
	/* the handle as returned by dlopen */
	void *libi2c;

	/* pointers to suitable i2c_send, i2c_recv, i2c_close functions */
	int (*send)  (struct i2cdev *handle, const unsigned char *data,
		      int len);
	int (*recv)  (struct i2cdev *handle, unsigned char *buf, int len);
	int (*close) (struct i2cdev *handle);

	/* the `struct i2cdev' pointer as returned by the real i2c_open */
	struct i2cdev *i2cdev;
};


/* open /dev/i2c device and set things up to be ready for i2c bus 
 * communication.
 * returns i2cdev handle pointer of i2c device or 0 on error
 */
struct i2cdev *
i2c_open(const char *device, int addr)
{
	const char *libkernel = "libi2c_kernel.so";
	const char *libusb = "libi2c_usb.so";
	const char *libethersex = "libi2c_ethersex.so";
	char *libakt;
	char *error;
	char *hostreg = "^.*:[0-9]*$";
	regex_t reg;
	
	regcomp(&reg, hostreg, REG_EXTENDED | REG_NOSUB);
	
	struct i2cdev* (*libi2c_open)(const char* device, int addr);
	
	if (strncmp("/dev/", device, 5) == 0)
	{
		libakt = (char *) libkernel;
	}
	else if(regexec(&reg, device, 0, 0, 0) == 0)
	{
		libakt = (char *) libethersex;
	}
	else
	{
		libakt = (char *) libusb;
	}
	
	regfree(&reg);
	
	struct i2cdev *handle;
	if((handle = malloc(sizeof(*handle))) == NULL){
		perror("malloc");
		return 0;
	}

	handle->libi2c = NULL;
	
	
	handle->libi2c = dlopen(libakt, RTLD_LAZY);
	if (!handle->libi2c) 
	{
		fprintf(stderr, "Fehler: dlopen(%s, RTLD_LAZY) %s\n", libakt, dlerror());
		return(0);
	}
	
	error = dlerror();
	
	libi2c_open = dlsym(handle->libi2c, "i2c_open");
	error = dlerror();
	if (error)
	{
	fprintf(stderr, "Fehler: dlsym(%p, \"i2c_open\") %s\n", handle->libi2c, error);
		return(0);
	}
	
	handle->send = dlsym(handle->libi2c, "i2c_send");
	error = dlerror();
	if (error)
	{
	fprintf(stderr, "Fehler: dlsym(%p, \"i2c_send\") %s\n", handle->libi2c, error);
		return(0);
	}
	
	handle->recv = dlsym(handle->libi2c, "i2c_recv");
	error = dlerror();
	if (error)
	{
	fprintf(stderr, "Fehler: dlsym(%p, \"i2c_recv\") %s\n", handle->libi2c, error);
		return(0);
	}
	
	handle->close = dlsym(handle->libi2c, "i2c_close");
	error = dlerror();
	if (error)
	{
	fprintf(stderr, "Fehler: dlsym(%p, \"i2c_close\") %s\n", handle->libi2c, error);
		return(0);
	}
	
	handle->i2cdev = libi2c_open(device, addr);
	if(handle->i2cdev == NULL)
	{
		fprintf(stderr, "Fehler: i2c_open fehlgeschlagen\n");
		return(0);
	}
	
	return (struct i2cdev*) handle;
}

/*close the device*/
int
i2c_close(struct i2cdev *ldhandle)
{
	if(ldhandle != NULL && ldhandle->i2cdev != NULL){
		ldhandle->close(ldhandle->i2cdev);
		dlclose(ldhandle->libi2c);
		free(ldhandle);
		return 1;
	}
	return -1;
}

/* send data to i2c device
 * return len on success, -1 on failure.
 */
int
i2c_send(struct i2cdev *ldhandle, const unsigned char *data, int len)
{
	if(ldhandle != NULL && ldhandle->i2cdev != NULL)
		return ldhandle->send(ldhandle->i2cdev, data, len);
	
	return -1; /* war ne NULL dabei */
}



/* read data from i2c device
 * return len on success, -1 on failure
 */
int
i2c_recv(struct i2cdev *ldhandle, unsigned char *buf, int len)
{
	if(ldhandle != NULL && ldhandle->i2cdev != NULL)
		return ldhandle->recv(ldhandle->i2cdev, buf, len);
	
	return -1; /* war ne NULL dabei */
}
