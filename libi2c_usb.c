/* Copyright(C) 2006,2007,2008 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/
 
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <usb.h>
#define LCKFILE "/dev/shm/usb2i2c.lck"

enum
{
	// Generic requests
	USBI2C_READ = 20,
	USBI2C_WRITE,
	USBI2C_STOP,
	USBI2C_STAT,
};

struct i2cdev { 
	usb_dev_handle *usbdev;
	int addr;
};

/* open usb2i2c adapter device and set things up to be ready for i2c bus
 * communication.
 */
struct i2cdev*
i2c_open(const char *device, int addr)
{
	struct i2cdev *handle;
	if((handle = malloc(sizeof(*handle))) == NULL){
		perror("malloc");
		return 0;
	}
	struct usb_bus *bus;
	unsigned int idVendor;
	unsigned int idProduct;
	if (sscanf(device, "%04x%04x", &idVendor, &idProduct) == 2)
	{
		usb_init();
		usb_find_busses();
		usb_find_devices();
		for (bus = usb_busses; bus; bus = bus->next)
		{
			struct usb_device *dev;
			for (dev = bus->devices; dev; dev = dev->next)
			{
				usb_dev_handle *usb_bus_dev;
				usb_bus_dev = usb_open(dev);
				if (usb_bus_dev)
				{
					if (dev->descriptor.idVendor == idVendor &&
									 dev->descriptor.idProduct == idProduct)
					{
						/* usb i2c adapter gefunden */
						//printf ("gefunden! devnr: %i %04X - %04X\n",1,
						//				dev->descriptor.idVendor, dev->descriptor.idProduct);
						handle->usbdev = usb_bus_dev;
						handle->addr = addr;
						/* wenn ein altes lock file vorhanden ist, dann loeschen */
						struct stat lckstat;
						if(stat(LCKFILE, &lckstat) == 0){
							if(lckstat.st_mtime < time(NULL)-10)
								unlink(LCKFILE);
						}
						return handle;
					}
				}
				if (usb_bus_dev)
					usb_close(usb_bus_dev);
			}
		}
	}
	fprintf(stderr,"keinen Adapter am usb gefunden\n");
	return 0;
}


/* detach the i2c bus and close usb device */
int
i2c_close(struct i2cdev *handle)
{
	if(handle != NULL)
	{
		usb_control_msg(handle->usbdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE |  USB_ENDPOINT_IN, USBI2C_STOP, 0, 0, 0, 0, 500);
		usb_close(handle->usbdev);
		unlink(LCKFILE);	
		return 1;
	}
	return -1;
}

/* send data to i2c device
 */
int
i2c_send(struct i2cdev *handle, const unsigned char *data, int len)
{
	int ret = -1;
	int lckfd;
	int maxtry = 0;
	do
	{
		lckfd = open(LCKFILE, O_CREAT|O_EXCL, S_IRUSR|S_IWUSR);
		usleep(10000);
	}
	while( lckfd < 0 && maxtry++ < 200);
	if(handle != NULL && maxtry < 200)
	{
		ret = usb_control_msg(handle->usbdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | 
				USB_ENDPOINT_OUT, USBI2C_WRITE, 0, handle->addr, (char*) data, len, 500);
		if ( ret == len )
		{
			char buf[16];
			ret = usb_control_msg(handle->usbdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | 
					USB_ENDPOINT_IN, USBI2C_STAT, 0, 0, (char*)&buf[0], 1, 500);
			if (ret == 1)
			{
				if(buf[0] != 0)
					ret = (int)buf[0]*-1;
				else
					ret = len;
			}
		}
		close(lckfd);
		unlink(LCKFILE);	
	}
	return ret; /* > 0 = ok */
}



/* read data from i2c device
 */
int
i2c_recv(struct i2cdev *handle, unsigned char *buf, int len)
{
	int ret = -1;
	int aktlen = len;
	int lckfd;
	int maxtry = 0;
	do
	{
		lckfd = open(LCKFILE, O_CREAT|O_EXCL, S_IRUSR|S_IWUSR);
		usleep(10000);
	}
	while( lckfd < 0 && maxtry++ < 200);
	if(handle != NULL && maxtry < 200)
	{
		ret = 0;
		do
		{
			ret += usb_control_msg(handle->usbdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE |  USB_ENDPOINT_IN, USBI2C_READ, 0, handle->addr, (char*)buf+len-aktlen, aktlen, 500);
			aktlen -= 255;
		}
		while(aktlen > 0);
		close(lckfd);
		unlink(LCKFILE);
	}
	return ret; /* > 0 = ok */
}
