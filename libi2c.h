/* Copyright(C) 2006,2007,2008 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/
 

#ifndef _LIBI2C
#define _LIBI2C

struct i2cdev; /* device handle zeiger */

/* open usb2i2c-adapter device and set things up to be ready for i2c bus
 * communication.
 * returns unix file descriptor of i2c device or 0 on error
 */
struct i2cdev*
i2c_open(const char *device, int addr);


/* detach the i2c bus and close usb device */
int
i2c_close(struct i2cdev *handle);

/* send data to i2c device
 * return len on success, <= 0 on failure.
 */
int
i2c_send(struct i2cdev *handle, const unsigned char *data, int len);

/* read data from i2c device 
 * return len on success, <= 0 on failure
 */
int
i2c_recv(struct i2cdev *handle, unsigned char *buf, int len);

#endif /* ... _I2C_USB */
