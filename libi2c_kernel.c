/* Copyright(C) 2006,2007,2008 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/
 

#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

struct i2cdev{
	int fd; 
	int addr; 
};

/* we must not include kernel headers in userspace
 * #include <linux/i2c.h>
 * #include <linux/i2c-dev.h>
 *
 * therefore we define the ioctl codes here ourselves ...
 */
#define I2C_SLAVE       0x0703  /* Change slave address                 */
                                /* Attn.: Slave address is 7 or 10 bits */
#define I2C_SLAVE_FORCE 0x0706  /* Change slave address                 */
                                /* Attn.: Slave address is 7 or 10 bits */
                                /* This changes the address, even if it */
                                /* is already taken!                    */
#define I2C_TENBIT      0x0704  /* 0 for 7 bit addrs, != 0 for 10 bit   */

#define I2C_FUNCS       0x0705  /* Get the adapter functionality */
#define I2C_RDWR        0x0707  /* Combined R/W transfer (one stop only)*/
#define I2C_PEC         0x0708  /* != 0 for SMBus PEC                   */


/* open /dev/i2c device and set things up to be ready for i2c bus 
 * communication.
 * returns i2cdev handle pointer of i2c device or 0 on error
 */
struct i2cdev*
i2c_open(const char *device, int addr)
{
	struct i2cdev *handle;
	if((handle = malloc(sizeof(*handle))) == NULL){
		perror("malloc");
		return 0;
	}
	
	if ((handle->fd = open(device, O_RDWR)) < 0) {
		perror("i2c_open");
		return 0;
	}
  
	/* When you have opened the device, you must specify with what device
   * address you want to communicate:
	*/
	if (ioctl(handle->fd, I2C_SLAVE, addr) < 0) {
		perror("i2c_open");
		return 0;
	}
	handle->addr = addr;
	
	return handle;
}

/*close the device*/
int
i2c_close(struct i2cdev *handle)
{
	if(handle != NULL){
		close(handle->fd);
		free(handle);
		return 1;
	}
	return -1;
}

/* send data to i2c device
 * return len on success, -1 on failure.
 */
int
i2c_send(struct i2cdev *handle, const unsigned char *data, int len)
{
	if(write(handle->fd, data, len) != len)
		return -1; /* didn't work. shall we emit an error message or the caller? */
	/* zum debugen write(2, buf, len +1);*/
  
	return len; /* hey, we rock */
}



/* read data from i2c device
 * return len on success, -1 on failure
 */
int
i2c_recv(struct i2cdev *handle, unsigned char *buf, int len)
{
	if(read(handle->fd, ((unsigned char *) buf), len) != len)
		return -1; /* stop. */
	return len;
}
